$(function () {
    $('.check-out-btn').click(checking);
    $('.check-in-btn').click(checking);

    $('.well').hide();
});

function checking(event) {
    var item_barcode = $('#itemBarcode').val();
    var location_barcode = $('#locationBarcode').val();

    if (item_barcode == "") {
        $('.well').html("Enter a valid item barcode.");
        $('.well').show();
    } else {
        $('.well').hide();
    }

    var username = $("#username").html();
    var status = $(this).html();

    var payload = {username: username, status: status, item_barcode: item_barcode, location_barcode: location_barcode};

    $.ajax({
        type: 'POST',
        url: '/activity/new',
        data: payload,
        success: function (data) {
            window.location.replace('/index.html');
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}