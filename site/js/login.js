$(function () {
    $('.sign-in-btn').click(signin);
    $('.well').hide();
});

function signin(event) {
    event.preventDefault();

    var name = $('input[name="name"]').val();

    var payload = {name: name, none: "1234"};

    $.ajax({
        type: 'POST',
        url: '/login',
        data: payload,
        success: function (data) {
            window.location.replace('/');
        },
        statusCode: {
            400: function () {
                $('.well').html('You forgot to fill in your name.');
                $('.well').show();
            }
        }
    });
}