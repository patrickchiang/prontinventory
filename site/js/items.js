$(function () {
    $('.add-item-btn').click(addItem);

    $('.edit-item-btn').click(editItem);

    $('.remove-item-btn').click(removeItem);
});

function removeItem(event) {
    var item_id = $(this).val();

    var payload = {id: item_id};

    $.ajax({
        type: 'POST',
        url: '/items/remove',
        data: payload,
        success: function (data) {
            window.location.replace('/items.html');
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}

function editItem(event) {
    event.preventDefault();

    $(this).html("Done");
    $(this).unbind("click");
    $(this).click(doneEditing);

    var parent = $(this).parent().parent();
    var name = parent.children(':nth-child(1)')[0];
    var desc = parent.children(':nth-child(2)')[0];
    var price = parent.children(':nth-child(3)')[0];
    var barcode = parent.children(':nth-child(4)')[0];

    var name_field = $('<input class="form-control" type="text" placeholder="Name"></input>').val($(name).html());
    $(name).empty().append(name_field);

    var desc_field = $('<input class="form-control" type="text" placeholder="Description"></input>').val($(desc).html());
    $(desc).empty().append(desc_field);

    var price_field = $('<input class="form-control right-align" type="text" placeholder="Price"></input>').val($(price).html());
    $(price).empty().append(price_field);

    var barcode_field = $('<input class="form-control right-align" type="text" placeholder="Barcode"></input>').val($(barcode).html());
    $(barcode).empty().append(barcode_field);
}

function doneEditing(event) {
    var item_id = $(this).val();

    var parent = $(this).parent().parent();
    var name = parent.children(':nth-child(1)');
    var desc = parent.children(':nth-child(2)');
    var price = parent.children(':nth-child(3)');
    var barcode = parent.children(':nth-child(4)');

    var name_value = name.children("input").val();
    var desc_value = desc.children("input").val();
    var price_value = price.children("input").val();
    var barcode_value = barcode.children("input").val();

    // replace field with text
    name.html(name_value);
    desc.html(desc_value);
    price.html(price_value);
    barcode.html(barcode_value);

    $(this).html("Edit");
    $(this).unbind("click");
    $(this).click(editItem);

    var payload = {id: item_id, name: name_value, desc: desc_value, price: price_value, barcode: barcode_value};

    $.ajax({
        type: 'POST',
        url: '/items/edit',
        data: payload,
        success: function (data) {
            console.log("change item success");
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}

function addItem(event) {
    event.preventDefault();

    var name = $('.add-item input[name="name"]').val();
    var desc = $('.add-item input[name="desc"]').val();
    var price = $('.add-item input[name="price"]').val();
    var barcode = $('.add-item input[name="barcode"]').val();

    var payload = {name: name, desc: desc, price: price, barcode: barcode};

    $.ajax({
        type: 'POST',
        url: '/items/new',
        data: payload,
        success: function (data) {
            window.location.replace('/items.html');
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}