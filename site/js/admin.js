$(function () {
    $('.sign-in-btn').click(signin);
    $('.well').hide();
});

function signin(event) {
    event.preventDefault();

    var username = $('input[name="username"]').val();
    var password = $('input[name="password"]').val();

    var payload = {username: username, password: password};

    $.ajax({
        type: 'POST',
        url: '/admin/login',
        data: payload,
        success: function (data) {
            window.location.replace('/');
        },
        statusCode: {
            400: function () {
                $('.well').html('You forgot to fill in your name.');
                $('.well').show();
            },
            401: function () {
                $('.well').html('Your email/pass combo was wrong.');
                $('.well').show();
            }
        }
    });
}