$(function () {
    $('.add-location-btn').click(addLocation);

    $('.edit-location-btn').click(editLocation);

    $('.remove-location-btn').click(removeLocation);
});

function removeLocation(event) {
    var location_id = $(this).val();

    var payload = {id: location_id};

    $.ajax({
        type: 'POST',
        url: '/locations/remove',
        data: payload,
        success: function (data) {
            window.location.replace('/locations.html');
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}

function editLocation(event) {
    event.preventDefault();

    $(this).html("Done");
    $(this).unbind("click");
    $(this).click(doneEditing);

    var parent = $(this).parent().parent();
    var name = parent.children(':nth-child(1)')[0];
    var desc = parent.children(':nth-child(2)')[0];
    var barcode = parent.children(':nth-child(3)')[0];

    var name_field = $('<input class="form-control" type="text" placeholder="Name"></input>').val($(name).html());
    $(name).empty().append(name_field);

    var desc_field = $('<input class="form-control" type="text" placeholder="Description"></input>').val($(desc).html());
    $(desc).empty().append(desc_field);

    var barcode_field = $('<input class="form-control right-align" type="text" placeholder="Barcode"></input>').val($(barcode).html());
    $(barcode).empty().append(barcode_field);
}

function doneEditing(event) {
    var location_id = $(this).val();

    var parent = $(this).parent().parent();
    var name = parent.children(':nth-child(1)');
    var desc = parent.children(':nth-child(2)');
    var barcode = parent.children(':nth-child(3)');

    var name_value = name.children("input").val();
    var desc_value = desc.children("input").val();
    var barcode_value = barcode.children("input").val();

    // replace field with text
    name.html(name_value);
    desc.html(desc_value);
    barcode.html(barcode_value);

    $(this).html("Edit");
    $(this).unbind("click");
    $(this).click(editLocation);

    var payload = {id: location_id, name: name_value, desc: desc_value, barcode: barcode_value};

    $.ajax({
        type: 'POST',
        url: '/locations/edit',
        data: payload,
        success: function (data) {
            console.log("change location success");
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}

function addLocation(event) {
    event.preventDefault();

    var name = $('.add-location input[name="name"]').val();
    var desc = $('.add-location input[name="desc"]').val();
    var barcode = $('.add-location input[name="barcode"]').val();

    var payload = {name: name, desc: desc, barcode: barcode};

    $.ajax({
        type: 'POST',
        url: '/locations/new',
        data: payload,
        success: function (data) {
            window.location.replace('/locations.html');
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}