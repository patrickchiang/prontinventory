$(function () {
    $('.add-admin-btn').click(addAdmin);
});

function addAdmin(event) {
    event.preventDefault();

    var username = $('.add-location input[name="username"]').val();
    var password = $('.add-location input[name="password"]').val();

    var payload = {username: username, password: password};

    $.ajax({
        type: 'POST',
        url: '/admin/new',
        data: payload,
        success: function (data) {
            window.location.replace('/');
        },
        statusCode: {
            400: function () {
                console.log("something went terribly wrong");
            }
        }
    });
}