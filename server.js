/**
 * Module imports
 */

var express = require('express');
var app = express();
var moment = require('moment');
var Sequelize = require('sequelize');

var models = require('./models');
var ensure = require('./routes/ensure.js');
var admin = require('./routes/admin.js');

app.set('views', __dirname + '/site');

/**
 * Routes
 */

require('./routes/auth.js')(app);
require('./routes/items.js')(app);
require('./routes/locations.js')(app);
require('./routes/activity.js')(app);
require('./routes/mobile.js')(app);

/**
 * Views
 */

app.get('/', function (req, res, next) {
    if (!req.user) {
        res.redirect('/login.html');
    } else {
        res.redirect('/index.html');
    }
});

function parseTime(i) {
    return moment(i).format("ddd, MMM D YYYY, h:mm:ss a");
}

app.get('/index.html', ensure, function (req, res) {
    res.render('index.jade', {user: req.user});
});

app.get('/newadmin.html', admin, function (req, res) {
    res.render('newadmin.jade', {user: req.user});
});

app.get('/activity.html', ensure, function (req, res) {
    models.Activity.findAll({
        include: [models.Item, models.Location]
    }).then(function (activities) {
        res.render('activity.jade', {activities: activities, user: req.user, parseTime: parseTime});
    });
});

app.get('/items.html', ensure, function (req, res) {
    models.Item.findAll().then(function (items) {
        var item = {};
        for (var i = 0; i < items.length; i++) {
            if (items[i].barcode == req.query.item) {
                item = items[i];
                break;
            }
        }

        if (Object.keys(item).length !== 0) {
            models.Activity.findAll({
                include: [{
                    model: models.Item, where: {
                        id: item.id
                    }
                }, models.Location],
                order: 'updatedAt DESC',
                limit: 10
            }).then(function (activities) {
                res.render('items.jade', {
                    items: items,
                    user: req.user,
                    item: item,
                    activities: activities,
                    parseTime: parseTime
                });
            });
        } else {
            res.render('items.jade', {items: items, user: req.user, item: item, parseTime: parseTime});
        }
    });
});

app.get('/locations.html', ensure, function (req, res) {
    models.Location.findAll().then(function (locations) {
        var location = {};
        for (var i = 0; i < locations.length; i++) {
            if (locations[i].barcode == req.query.location) {
                location = locations[i];
                break;
            }
        }

        if (Object.keys(location).length !== 0) {
            models.Activity.findAll({
                include: [
                    models.Item,
                    {
                        model: models.Location, where: {
                        id: location.id
                    }
                    }],
                order: 'updatedAt DESC',
                limit: 10
            }).then(function (activities) {
                res.render('locations.jade', {
                    locations: locations,
                    user: req.user,
                    location: location,
                    activities: activities,
                    parseTime: parseTime
                });
            });
        } else {
            res.render('locations.jade', {
                locations: locations,
                user: req.user,
                location: location,
                parseTime: parseTime
            });
        }
    });
});

app.get('/init', function (req, res) {
    models.sequelize.sync({force: true});
    res.json('done')
});

/**
 * Static views
 */

app.use('/', express.static(__dirname + '/site'));

/**
 * Initialize server
 */

var server = app.listen(3000, function () {
    console.log('Listening on port %d', server.address().port);
});