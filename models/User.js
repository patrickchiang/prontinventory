module.exports = function (sequelize, Sequelize) {
    var User = sequelize.define('User', {
        user_username: {
            type: Sequelize.STRING,
            allowNull: false,   // TODO: change
            unique: true
        },
        user_password: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        freezeTableName: true
    });

    return User;
};