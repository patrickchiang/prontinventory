module.exports = function (sequelize, Sequelize) {
    var Item = sequelize.define('Item', {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        desc: {
            type: Sequelize.STRING,
            allowNull: true
        },
        price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        barcode: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        freezeTableName: true
    });

    return Item;
};