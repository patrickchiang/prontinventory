module.exports = function (sequelize, Sequelize) {
    var Activity = sequelize.define('Activity', {
        checking_status: {
            type: Sequelize.STRING,
            allowNull: false
        },
        user_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        item_id: {
            type: Sequelize.INTEGER,
            references: 'Item',
            referencesKey: 'id'
        },
        location_id: {
            type: Sequelize.INTEGER,
            references: 'Location',
            referencesKey: 'id'
        }
    }, {
        freezeTableName: true,
        classMethods: {
            associate: function (models) {
                Activity.belongsTo(models.Item, {foreignKey: 'item_id', onDelete: 'cascade'});
                Activity.belongsTo(models.Location, {foreignKey: 'location_id', onDelete: 'cascade'});
            }
        }
    });

    return Activity;
};