module.exports = function (sequelize, Sequelize) {
    var Location = sequelize.define('Location', {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        desc: {
            type: Sequelize.STRING,
            allowNull: true
        },
        barcode: {
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {
        freezeTableName: true
    });

    return Location;
};