module.exports = function (app) {
    var models = require('../models');
    var ensure = require('./ensure.js');
    var admin = require('./admin.js');

    app.get('/items', ensure, function (req, res) {
        models.Item.findAll().then(function (items) {
            res.json(items);
        });
    });

    app.post('/items/new', admin, function (req, res) {
        var name = req.body.name;
        var desc = req.body.desc;
        var barcode = req.body.barcode;
        var price = req.body.price || 0;

        models.Item.create({
            name: name,
            desc: desc,
            barcode: barcode,
            price: price
        }).then(function (item) {
            item.save().then(function () {
                res.json("success");
            });
        });
    });

    app.post('/items/edit', admin, function (req, res) {
        var id = req.body.id;

        var name = req.body.name;
        var desc = req.body.desc;
        var barcode = req.body.barcode;
        var price = req.body.price || 0;

        // find the id
        models.Item.find(id).then(function (item) {
            if (!item) {
                res.json("fail");
                return;
            }

            // update it
            item.name = name;
            item.desc = desc;
            item.barcode = barcode;
            item.price = price;

            // save it
            item.save().then(function () {
                res.json("success");
            });
        });
    });

    app.post('/items/remove', admin, function (req, res) {
        var id = req.body.id;

        // find the id
        models.Item.find(id).then(function (item) {
            if (!item) {
                res.json("fail");
                return;
            }
            item.destroy().then(function () {
                res.json("success");
            });
        });
    });

    app.get('/item/:barcode', function (req, res) {
        var barcode = req.params.barcode;

        models.Item.findAll({
            where: {
                barcode: barcode
            }
        }).then(function (item) {
            res.json(item);
        });
    });
};