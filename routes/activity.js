module.exports = function (app) {
    var models = require('../models');
    var ensure = require('./ensure.js');

    app.post('/activity/new', function (req, res) {
        var username = req.body.username;
        var status = req.body.status;
        var item_barcode = req.body.item_barcode;
        var location_barcode = req.body.location_barcode;

        var item_id;

        models.Item.find({
            where: {
                barcode: item_barcode
            }
        }).then(function (item) {
            item_id = item.id;
            return models.Location.find({
                where: {
                    barcode: location_barcode
                }
            });
        }).then(function (location) {
            if (location != null) {
                var location_id = location.id;

                return models.Activity.create({
                    user_name: username,
                    checking_status: status,
                    item_id: item_id,
                    location_id: location_id
                });
            } else {
                return models.Activity.create({
                    user_name: username,
                    checking_status: status,
                    item_id: item_id
                });
            }
        }).then(function (activity) {
            return activity.save();
        }).then(function () {
            res.json("success");
        });
    });
};