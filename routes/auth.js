module.exports = function (app) {

    var bcrypt = require('bcrypt-nodejs');
    var passport = require('passport'), LocalStrategy = require('passport-local').Strategy;
    var session = require('express-session');
    var bodyParser = require('body-parser');

    var models = require('../models');
    var admin = require('./admin.js');

    /**
     * Sessions setup
     */

    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    app.use(session({
        secret: 'patrick is the best as always',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());

    /**
     * Passport serialization
     */

    passport.serializeUser(function (user, done) {
        done(null, user);
    });
    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    /**
     * Passport local strategy users setup
     */

    passport.use('local-user', new LocalStrategy({
            usernameField: 'name',
            passwordField: 'none'
        },
        function (username, password, done) {
            if (username != null && username != "") {
                return done(null, {
                    name: username
                });
            }

            return done(null, false);
        }
    ));

    passport.use('local-admin', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        },
        function (username, password, done) {
            models.User.findAll().then(function (users) {
                console.log(users);
                if (users == null || users.length == 0) {
                    models.User.create({
                        user_username: username,
                        user_password: bcrypt.hashSync(password)
                    }).then(function (user) {
                        user.save().then(function () {
                            return done(null, {
                                name: username,
                                admin: true
                            });
                        });
                    });
                } else {
                    models.User.find({
                        where: {user_username: username}
                    }).then(function (user) {
                        var results = user;

                        if (results == null)
                            return done(null, false);
                        if (bcrypt.compareSync(password, results.user_password)) {
                            var user = results;
                            return done(null, {
                                name: user.user_username,
                                admin: true
                            });
                        } else {
                            return done(null, false);
                        }
                    }).error(function (err) {
                        return done(null, false);
                    });
                }
            });
        }
    ));


    /**
     * Routing
     */

    app.post('/login', passport.authenticate('local-user'), function (req, res) {
        res.json(req.user);
    });

    app.post('/admin/login', passport.authenticate('local-admin'), function (req, res) {
        res.json(req.user);
    });

    app.post('/admin/new', admin, function (req, res) {
        models.User.create({
            user_username: req.body.username,
            user_password: bcrypt.hashSync(req.body.password)
        }).then(function (user) {
            user.save().then(function () {
                res.json("success");
            });
        });
    });

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/login.html');
    });

};