module.exports = function (app) {
    var models = require('../models');
    var ensure = require('./ensure.js');
    var admin = require('./admin.js');

    app.get('/mobile/index', ensure, function (req, res) {
        var payload = {user: req.user};
        res.json(payload);
    });
};