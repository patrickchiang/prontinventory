module.exports = function (app) {
    var models = require('../models');
    var ensure = require('./ensure.js');
    var admin = require('./admin.js');

    app.get('/locations', ensure, function (req, res) {
        models.Location.findAll().then(function (locations) {
            res.json(locations);
        });
    });

    app.post('/locations/new', admin, function (req, res) {
        var name = req.body.name;
        var desc = req.body.desc;
        var barcode = req.body.barcode;

        models.Location.create({
            name: name,
            desc: desc,
            barcode: barcode
        }).then(function (location) {
            location.save().then(function () {
                res.json("success");
            });
        });
    });

    app.post('/locations/edit', admin, function (req, res) {
        var id = req.body.id;

        var name = req.body.name;
        var desc = req.body.desc;
        var barcode = req.body.barcode;

        // find the id
        models.Location.find(id).then(function (location) {
            if (!location) {
                res.json("fail");
                return;
            }

            // update it
            location.name = name;
            location.desc = desc;
            location.barcode = barcode;

            // save it
            location.save().then(function () {
                res.json("success");
            });
        });
    });

    app.post('/locations/remove', admin, function (req, res) {
        var id = req.body.id;

        // find the id
        models.Location.find(id).then(function (location) {
            if (!location) {
                res.json("fail");
                return;
            }
            location.destroy().then(function () {
                res.json("success");
            });
        });
    });

    app.get('/location/:barcode', function (req, res) {
        var barcode = req.params.barcode;

        models.Location.findAll({
            where: {
                barcode: barcode
            }
        }).then(function (location) {
            res.json(location);
        });
    });
};